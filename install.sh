#!/bin/bash

# docker volume rm $(docker volume ls -q --filter dangling=true)

if ! [ -f rs_keyfile ]; then
  openssl rand -base64 756 > rs_keyfile
  chmod 0400 rs_keyfile
  sudo chown 999:999 rs_keyfile
fi

docker compose up -d

echo "Wait 5 seconds"
sleep 5

docker exec -it mongo_1 mongosh -u root -p password --eval "rs.initiate({ _id: 'rs0', version: 1, members: [{ _id: 0, host: 'mongo_1:27017' }, { _id: 1, host: 'mongo_2:27018' },{ _id: 2, host: 'mongo_3:27019' }]})"


