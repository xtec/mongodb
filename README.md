# Mongodb

Aquest projecte correspon a l'activitat <https://xtec.dev/data/mongodb>


Executa l'script per instal.lar un conjunt de répliques amb [Docker](https://xtec.dev/linux/docker/).

```sh
$ ./install.sh
```

La instal.lació crea un usuari administrador amb el nom de `root` i contrasenya `password`.

Et pots conectar al node `mongo_1` directament amb:

```sh
docker run -it --rm --network host mongo:8 mongosh -u root -p password
```
